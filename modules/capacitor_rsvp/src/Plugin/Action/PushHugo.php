<?php

namespace Drupal\capacitor_rsvp\Plugin\Action;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Action\Plugin\Action\EntityActionBase;
use Drupal\Component\Serialization\Json;
use Cz\Git\GitRepository;

/**
 * Push to hugo site.
 *
 * @Action(
 *   id = "push_rsvp_hugo",
 *   label = @Translation("Push RSVP to hugo site"),
 *   type = "node"
 * )
 */
class PushHugo extends EntityActionBase {

  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute($object = NULL) {
    exec('rm -rf /app/web/sites/default/files/repos');
    GitRepository::cloneRepository('https://gitlab.com/hsbxl/site.git', '/app/web/sites/default/files/repos');

    $attendees = [
      [
        'name' => 'betz',
        'date' => '2019-02-23 21:25:13 +0100',
      ],
      [
        'name' => 'foobar',
        'date' => '2019-02-21 21:25:13 +0100',
      ],
      [
        'name' => 'John Doe',
        'date' => '2019-02-20 21:25:13 +0100',
      ],
    ];


    $json = Json::encode($attendees);
    $filename = md5('events/merda_elettronica/Attack_Delay_Workshop.md') . ".json";

    drupal_mkdir('/app/web/sites/default/files/repos/data/rsvp');
    file_unmanaged_save_data($json, '/app/web/sites/default/files/repos/data/rsvp/' . $filename, FILE_EXISTS_REPLACE);

    $repo = new GitRepository('/app/web/sites/default/files/repos');
    $repo->pull('origin');
    $repo->addFile('/app/web/sites/default/files/repos/data/rsvp/' . $filename);

    if($repo->hasChanges()) {
      $repo->commit('test rsvp', array('--author' => 'capacitor <tom@behets.me>'));
      $repo->push('origin');
      drupal_set_message("RSVP committed as " . $filename . ".");
    }
    else {
      drupal_set_message("No changes. Nothing to commit.");
    }
  }
}

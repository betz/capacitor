<?php

namespace Drupal\capacitor_rsvp\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Component\Utility\Html;
use Drupal\capacitor_rsvp\Services\rsvp_service;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a RSVP resource
 *
 * @RestResource(
 *   id = "rsvp_resource",
 *   label = @Translation("RSVP Resource"),
 *   uri_paths = {
 *     "canonical" = "/capacitor/rsvp",
 *     "https://www.drupal.org/link-relations/create" = "/capacitor/rsvp"
 *   }
 * )
 */
  class rsvp extends ResourceBase {


    /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get() {
    //$rsvp_service = new rsvp_service();
    $response = ['message' => 'Hello, this is a rest GET service'];
    return new ResourceResponse($response);
  }

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(array $data) {
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'event');
    $query->condition('field_content_path', $data['content_path']);
    $query->sort('nid');

    // If multiple events found, only use the last one.
    $nids = $query->execute();
    $nid = array_pop($nids);

    if(!$nid)
    {
      // No event found, create one.
      $node = Node::create(['type'  => 'event']);
      $node->set('title', $data['event_name']);
      $node->set('field_content_path', $data['content_path']);
      $node->set('field_event_id', $data['content_path']);
      $node->save();
      $nid = $node->nid->value;
    } else {
      // event found, load node
      // and overwrite data with last submitted fields.
      $node = entity_load('node', $nid);
      $node->set('title', $data['event_name']);
      $node->set('field_content_path', $data['content_path']);
      $node->set('field_event_id', $data['content_path']);
      $node->save();
    }

    if(empty($data['rsvp_nickname'])) {
      return new ResourceResponse(['message' => 'Please enter your (nick)name. This will be public.']);
    }

    if(!\Drupal::service('email.validator')->isValid($data['rsvp_email'])) {
      return new ResourceResponse(['message' => 'Please enter a valid email. This will not be public.']);
    }

    // Create RSVP paragraph.
    $rsvp = Paragraph::create(['type' => 'rsvp']);
    $rsvp->set('field_rsvp_email', $data['rsvp_email']);
    $rsvp->set('field_rsvp_name', $data['rsvp_nickname']);
    $rsvp->save();

    $node->field_rsvp[] = [
      'target_id' => $rsvp->id(),
      'target_revision_id' => $rsvp->getRevisionId(),
    ];
    $node->save();

    $existing = $node->get('field_rsvp');

    $a = 0;

    $message = Html::escape($rsvp->field_rsvp_name->value);

    return new ResourceResponse(['message' => $rsvp->field_rsvp_name->value, 'event_name' => $data['event_name']]);
  }
}

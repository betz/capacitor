<?php

namespace Drupal\capacitor_rsvp\Services;

use \Drupal\Core\Database\Connection;

/**
 * Class rsvp.
 */
class rsvp_service {
  /*
   * @var \Drupal\Core\Database\Connection $database
   */
  protected $database;

  /**
   * Constructs a new rsvp object.
   * @param \Drupal\Core\Database\Connection $connection
   */
  public function __construct() {
    $connection = \Drupal\Core\Database\Database::getConnection();
    $this->database = $connection;
  }

  public function checkEvent () {
    $query = $this->database->query('SELECT nid FROM {node}');
    $result = $query->fetchAssoc();
    $a = 0;
    return $result;
  }
}
